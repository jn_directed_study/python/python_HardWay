#
# Python 3.7.0-slim-stretch
#
FROM python:3.7.0-slim-stretch

#
# Set Timezone
#
ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#
# Copy Requirements
#
COPY lib/ /pkg/

#
# Install Prerequisites
#
RUN pip install -r /pkg/requirements.txt

#
# Create a Volume
#
VOLUME /lpthw

#
# Change WorkDir
#
WORKDIR /lpthw

