import re
import base64

cleanerText = 'O serviço {64bsU1JWeyp9} ("Qualquer") pode cruzar seus segmentos de rede internos. {64bsUVJFRntxX3Nydl9JbnNpZGVfSW5zaWRlOip9} Número de endereços IP internos que possuem acesso: {64bsTl9TUkNfSU1QQUNUX0lQUw ==} Número de endereços internos acessíveis: {64bsTl9EU1RfSU1QQUNUX0lQUw ==} Permitir que o serviço "Qualquer" entre diferentes segmentos de rede seja arriscado, pois o serviço "Qualquer" inclui muitos vulneráveis Serviços. Isso significa que os diferentes segmentos de rede não estão devidamente separados uns dos outros. O risco é maior se o serviço "Qualquer" for permitido de uma rede DMZ em outros segmentos de rede: Se um servidor DMZ for infectado por um worm ou vírus, a infecção pode se espalhar para outras partes da rede.'
#a = 'ola tudo bem'
#b = re.compile('%(.*?) ')
#c = re.sub(b, '', a)
myField = re.findall('{64bs(.*?)}', cleanerText)
if not myField :
    print('none')
else:
    for myCount in range(0, len(myField)):
        cleanerText = cleanerText.replace('{64bs' + myField[myCount] + '}', '%'+ base64.b64decode(myField[myCount]).decode('ascii'))

print(cleanerText)


"""
while myLoop == True:
if '{64bs' in cleanerText:
    myField = re.search('{64bs(.*?)}', cleanerText)
    cleanerText = cleanerText.replace(myField.group(0), base64.b64decode(myField.group(0).replace(' ','')[5:-1]).decode('ascii'))
else:
    myLoop = False
"""